import { Button, Container, Grid, InputLabel, MenuItem, Pagination, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect, useState } from "react"
import ModelUser from "./model";

function DataTable() {
    // Nên làm lại task này theo redux làm thế này hơi chưa đúng convention 
    //để hiểu rõ hơn về redux
    const [numberRow, setNumber] = useState(10) // số dòng trong 1 trang

    const [rows, setRows] = useState([]);

    const [TotalPage, setTotalPage] = useState(0);

    const [curPage, setCurPage] = useState(1)

    const [openModal, setOpenModal] = useState(false)

    const [userInfo, setUserInfo] = useState([])

    const onClickOpenModalHandler = (row) => {
        setOpenModal(true)
        setUserInfo(row)
        console.log(row)
    }

    const onClickCloseModalHandler = () => {
        setOpenModal(false)
    }


    const fetchAPI = async (url) => {
        const res = await fetch(url);
        const data = res.json();
        return data;
    }

    const handleChangePage = (event, value) => {
        setCurPage(value)
    }

    const OnPageChangeHandler = (event) => {
        setNumber(event.target.value)
    }

    useEffect(() => {
        fetchAPI("http://203.171.20.210:8080/devcamp-register-java-api/users")
            .then((data) => {
                setTotalPage(data.length / numberRow);
                //setRows(data);
                setRows(data.slice((curPage - 1) * numberRow, curPage * numberRow))
            })
            .catch((error) => {
                console.error(error.message)
            })
    }, [curPage, numberRow])// cho numberRow vào đây thì hoạt động hahaha

    return (
        <Container>
            <Grid container mt={5}>
                <Grid container>
                    <Grid item lg={12}>
                        <InputLabel id="demo-simple-select-label">Select</InputLabel>
                    </Grid>
                    <Grid item lg={10}>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={numberRow}
                            //label="Page"
                            onChange={OnPageChangeHandler}
                            style={{ width: "100px" }}
                        >
                            <MenuItem value={5}>Five</MenuItem>
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={15}>Fifteen</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                        </Select>
                    </Grid>

                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Mã người dùng</TableCell>
                                    <TableCell>firstname</TableCell>
                                    <TableCell>lastname</TableCell>
                                    <TableCell>country</TableCell>
                                    <TableCell>subject</TableCell>
                                    <TableCell>customerType</TableCell>
                                    <TableCell>registerStatus</TableCell>
                                    <TableCell>Action</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    rows.map((row, id) => {
                                        return (
                                            <TableRow key={id}>
                                                <TableCell>{row.id}</TableCell>
                                                <TableCell>{row.firstname}</TableCell>
                                                <TableCell>{row.lastname}</TableCell>
                                                <TableCell>{row.country}</TableCell>
                                                <TableCell>{row.subject}</TableCell>
                                                <TableCell>{row.customerType}</TableCell>
                                                <TableCell>{row.registerStatus}</TableCell>
                                                <TableCell><Button onClick={() => onClickOpenModalHandler(row)}>Chi tiết</Button></TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item >
                    <Pagination count={TotalPage} defaultPage={curPage} onChange={handleChangePage}>
                    </Pagination>
                </Grid>
            </Grid>
            <Grid>
                <ModelUser
                    openProps={openModal}
                    closeProps={onClickCloseModalHandler}
                    userInfoProps={userInfo}
                >
                </ModelUser>
            </Grid>

        </Container>
    )
}

export default DataTable

import './App.css';
import DataTable from './component/DataTable';

function App() {
  return (
    <div>
      <DataTable></DataTable>
    </div>
  );
}

export default App;
